package br.com.proway.teclado.Controller;

import br.com.proway.teclado.model.Teclado;
import br.com.proway.teclado.repository.TecladoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class TecladoController {

    @Autowired
    private TecladoRepository tecladoRepository;

    @GetMapping("/teclado/{id}")
    public Teclado getTeclado(@PathVariable("id") long id) {
        return tecladoRepository.findById(id);
    }

    @GetMapping("/teclados")
    public ArrayList<Teclado> getTeclado() {
        return tecladoRepository.findAll();
    }

    @GetMapping("/tecladobymarca/{marca}")
    public ArrayList<Teclado> getTecladoByMarca(@PathVariable("marca") String marca) {
        return tecladoRepository.findByMarca(marca);
    }

    @GetMapping("/teclado-tpm/{tpm}")
    public ArrayList<Teclado> getTecladoTpm(@PathVariable("tpm") long tpm) {
        return tecladoRepository.findTecladoSwitch(tpm);
    }

    @PutMapping("/teclado/{id}")
    public Teclado putTeclado(@PathVariable("id") long id, @RequestBody Teclado teclado) {
        teclado.setId(id);
        return tecladoRepository.save(teclado);
    }

    @PostMapping("/new-teclado")
    public ResponseEntity<Teclado> postTeclado(@RequestBody Teclado teclado) {
        Teclado t;
        try {
            t = tecladoRepository.save(teclado);
            return new ResponseEntity<>(t, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/teclado/{id}")
    public void delTeclado(@PathVariable("id") long id) {
        tecladoRepository.deleteById(id);
    }
}
