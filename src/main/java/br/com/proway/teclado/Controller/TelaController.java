package br.com.proway.teclado.Controller;

import br.com.proway.teclado.model.Tela;
import br.com.proway.teclado.repository.TelaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class TelaController {

    @Autowired
    private TelaRepository telaRepository;

    @GetMapping("/tela/{id}")
    public Tela getTela(@PathVariable("id") long id) {
        return telaRepository.findById(id);
    }

    @GetMapping("/telas")
    public ArrayList<Tela> getTela() {
        return telaRepository.findAll();
    }

    @GetMapping("/telabymarca/{marca}")
    public ArrayList<Tela> getTelaByMarca(@PathVariable("marca") String marca) {
        return telaRepository.findByMarca(marca);
    }

    @GetMapping("/tela-polegada/{polegada}")
    public ArrayList<Tela> getByGood(@PathVariable("polegada") long polegada) {
        return telaRepository.findByGoodTela(polegada);
    }

    @PutMapping("/tela/{id}")
    public Tela putTela(@PathVariable("id") long id, @RequestBody Tela tela) {
        tela.setId(id);
        return telaRepository.save(tela);
    }

    @PostMapping("/new-tela")
    public ResponseEntity<Tela> postTela(@RequestBody Tela tela) {
        Tela t;
        try {
            t = telaRepository.save(tela);
            return new ResponseEntity<>(t, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/tela/{id}")
    public void delTela(@PathVariable("id") long id) {
        telaRepository.deleteById(id);
    }
}
