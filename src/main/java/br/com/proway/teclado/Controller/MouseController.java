package br.com.proway.teclado.Controller;

import br.com.proway.teclado.model.Mouse;
import br.com.proway.teclado.repository.MouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class MouseController {

    @Autowired
    private MouseRepository mouseRepository;

    @GetMapping("/mouse/{id}")
    public Mouse getMouse(@PathVariable("id") long id) {
        return mouseRepository.findById(id);
    }

    @GetMapping("/mouses")
    public ArrayList<Mouse> getMouses() {
        return mouseRepository.findAll();
    }

    @GetMapping("/mousebymarca/{marca}")
    public ArrayList<Mouse> getMouseByMarca(@PathVariable("marca") String marca) {
        return mouseRepository.findByMarca(marca);
    }

    @GetMapping("/dpimaior/{dpi}")
    public ArrayList<Mouse> getDpi(@PathVariable("dpi") long dpi) {
        return mouseRepository.findByMaior(dpi);
    }

    @PutMapping("/mouse/{id}")
    public Mouse putMouse(@PathVariable("id") long id, @RequestBody Mouse mouse) {
        mouse.setId(id);
        return mouseRepository.save(mouse);
    }

    @PostMapping("/new-mouse")
    public ResponseEntity<Mouse> postMouse(@RequestBody Mouse mouse) {
        Mouse m;
        try {
            m = mouseRepository.save(mouse);
            return new ResponseEntity<>(m, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/mouse/{id}")
    public void delMouse(@PathVariable("id") long id) {
        mouseRepository.deleteById(id);
    }
}
