package br.com.proway.teclado.repository;

import br.com.proway.teclado.model.Mouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface MouseRepository extends JpaRepository<Mouse, Long> {


    ArrayList<Mouse> findAll();

    Mouse findById(long id);

    Mouse save(Mouse mouse);

    void deleteById(long id);

    ArrayList<Mouse> findByMarca(String marca);

    @Query("SELECT m FROM Mouse m WHERE m.dpi > :dpi")
    ArrayList<Mouse> findByMaior(@Param("dpi") long dpi);
}
