package br.com.proway.teclado.repository;

import br.com.proway.teclado.model.Teclado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface TecladoRepository extends JpaRepository<Teclado, Long> {


    ArrayList<Teclado> findAll();

    Teclado findById(long id);

    Teclado save(Teclado teclado);

    void deleteById(long id);

    ArrayList<Teclado> findByMarca(String teclado);

    @Query("SELECT t FROM Teclado t WHERE t.tpm >= :tpm ORDER BY t.tpm DESC")
    ArrayList<Teclado> findTecladoSwitch(@Param("tpm") long tpm);
}
