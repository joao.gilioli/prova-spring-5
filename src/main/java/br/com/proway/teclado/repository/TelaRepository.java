package br.com.proway.teclado.repository;

import br.com.proway.teclado.model.Tela;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface TelaRepository extends JpaRepository<Tela, Long> {

    ArrayList<Tela> findAll();

    Tela findById(long id);

    Tela save(Tela tela);

    void deleteById(long id);

    ArrayList<Tela> findByMarca(String marca);

    @Query("SELECT t FROM Tela t WHERE t.hertz >= 144 AND t.polegada >= :polegada")
    ArrayList<Tela> findByGoodTela(@Param("polegada") long polegada);

}
