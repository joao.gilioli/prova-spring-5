package br.com.proway.teclado.model;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "teclado")
@EntityListeners(AuditingEntityListener.class)
public class Teclado {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "marca")
    private String marca;
    @Column(name = "tecla_switch")
    private String teclaSwitch;
    @Column(name = "tpm")
    private Long tpm;
}
