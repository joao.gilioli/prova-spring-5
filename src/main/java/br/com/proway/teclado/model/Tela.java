package br.com.proway.teclado.model;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "tela")
@EntityListeners(AuditingEntityListener.class)
public class Tela {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "marca")
    private String marca;
    @Column(name = "polegada")
    private Long polegada;
    @Column(name = "hertz")
    private Long hertz;
}
