package br.com.proway.teclado.model;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "mouse")
@EntityListeners(AuditingEntityListener.class)
public class Mouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "marca")
    private String marca;
    @Column(name = "dpi")
    private Long dpi;
}
